variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "jcalman9@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "438028467474.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "438028467474.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "django_secret_key" {
  description = "Secret key for django app"
}

variable "s3_prefix" {
  description = "Prefix fo the S3 bucket"
  default     = "jimalman"
}